aed - Attach External Database

Module originally created by Kevin Finkenbinder
May 1, 2013 - June 10, 2013
(c) Michigan State University Board of Trustees 
Licensed under GNU General Public License (GPL) Version 2.

COPYRIGHT © 2013 
MICHIGAN STATE UNIVERSITY BOARD OF TRUSTEES 
ALL RIGHTS RESERVED

PERMISSION IS GRANTED TO USE, COPY, CREATE DERIVATIVE WORKS AND
REDISTRIBUTE THIS SOFTWARE AND SUCH DERIVATIVE WORKS FOR ANY
PURPOSE, SO LONG AS THE NAME OF MICHIGAN STATE UNIVERSITY IS
NOT USED IN ANY ADVERTISING OR PUBLICITY PERTAINING TO THE USE
OR DISTRIBUTION OF THIS SOFTWARE WITHOUT SPECIFIC, WRITTEN
PRIOR AUTHORIZATION.  IF THE ABOVE COPYRIGHT NOTICE OR ANY
OTHER IDENTIFICATION OF MICHIGAN STATE UNIVERSITY IS INCLUDED
IN ANY COPY OF ANY PORTION OF THIS SOFTWARE, THEN THE
DISCLAIMER BELOW MUST ALSO BE INCLUDED.

THIS SOFTWARE IS PROVIDED  _AS_IS,_WITHOUT_REPRESENTATION_FROM_
_MICHIGAN_STATE_UNIVERSITY_AS_TO_ITS_FITNESS_FOR_ANY_PURPOSE,_
_AND_WITHOUT_WARRANTY_BY_MICHIGAN_STATE_UNIVERSITY_OF_ANY_KIND_,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE. THE MICHIGAN STATE UNIVERSITY BOARD OF
TRUSTEES SHALL NOT BE LIABLE FOR ANY DAMAGES, INCLUDING SPECIAL,
INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, WITH RESPECT TO
ANY CLAIM ARISING OUT OF OR IN CONNECTION WITH THE USE OF THE
SOFTWARE, EVEN IF IT HAS BEEN OR IS HEREAFTER ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.


Summary:  This module is designed for developers to quickly add to or
remove databases from settings.php.  This module cannot manage
any database that was hand entered into settings.php, but only 
the ones that it has placed there.

Installation: Installation of this module proceeds as normal.  Once this module
is installed, there will be a new menu entry under 
admin/configuration/development/aed.

Usage:
1) The top section of the page lists all databases that are
currently defined in the $databases array.  If an item in this
list is managed by aed, its listing will be followed by the words
"managed by aed".  It a database is not listed in this section,
it is not accessible by other modules.

2) The second section of the page lists all databases that are
managed by aed.  (By default, this section is empty until at
least one database is added using the procedure in section 3
below.)  To edit a database in this section, click on its name
(see section 4 for more details).

3) The final section of this page allows authorized users to add
a database into aed by filling out the form.  The form is
responsive and displays the appropriate fields for each supported
database format (mysql, mysqli, pgsql, sqlite).  After filling
in the necessary information, click on "Save Settings and Add
this Database".

4) To edit a database or add it to the "installed" list in
section 1, click on the name of the database in section 2.
4.1) If the database has not been added to settings.php via aed,
you will have four (4) options:
    delete - remove the database from aed management and the
              internal aed database.
    update - if any of the fields defining the database have been
              changed, then this saves those changes into aed
              tracking database.  NOTE: you MUST put in the
              database password for any change you make to the
              database definition.
    add to settings.php - adds the database into the settings.php
              file and makes it available via the $databases
              variable.
    cancel - return to the main aed screen without making any
              changes.
4.2) If the database has been added to settings.php via aed, you
will have two (2) options:
    remove from settings.php - remove the database from the
              settings.php file and make it unavailable in the
              $databases variable.
    cancel - return to the main aed screen without making any
              changes.
