<?php

/**
 * @file
 * Contains functions to implement various forms and callbacks.
 */

/**
 * Form Builder; edit a database stored in aed.
 * 
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function aed_edit_form($form, &$form_state, $focus_database) {
  $form[] = array(
    '#markup' => t("Edit/Delete the '<strong>@dbname</strong>' database", array(
      '@dbname' => $focus_database)),
    '#prefix' => '<h3>',
    '#suffix' => '</h3>',
  );

  $query = db_select('aed', 'a')
      ->fields('a', array('DatabaseHandle', 'DatabaseConnectionInfo',
        'in_settings_php'))
      ->condition('DatabaseHandle', $focus_database, '=');
  $qr = $query->execute();

  if ($qr->rowCount() === 1) {
    $dbsettings = $qr->fetchobject();

    $form[] = array(
      '#prefix' => '<h3>',
      '#markup' => $dbsettings->DatabaseHandle,
      '#suffix' => '</h3>',
    );

    $form['aed_old_database'] = array(
      '#type' => 'hidden',
      '#value' => $dbsettings->DatabaseHandle,
    );
    $definition = unserialize($dbsettings->DatabaseConnectionInfo);

    // Display form to edit/delete databases.
    $form['aed_database'] = array(
      '#type' => 'machine_name',
      '#title' => t('PHP name (machine_name) of Database'),
      '#description' => t('The name of the database as it will be known in php code.'),
      '#size' => 40,
      '#maxlength' => 60,
      '#required' => TRUE,
      '#default_value' => $dbsettings->DatabaseHandle,
      '#machine_name' => array(
        'exists' => 'aed_name_exists',
      ),
    );
    // Text field for the database driver.
    $form['aed_driver'] = array(
      '#type' => 'select',
      '#title' => t('Database Driver'),
      '#description' => t('Driver to use in accessing the database.'),
      '#options' => array(
        'mysql' => t('mysql'),
        'mysqli' => t('mysqli'),
        'pgsql' => t('pgsql'),
        'sqlite' => t('sqlite'),
      ),
      '#default_value' => $definition['driver'],
      '#required' => TRUE,
    );

    // Text field for the database name on the server.
    $form['aed_db'] = array(
      '#type' => 'textfield',
      '#title' => t('Server name of Database to register'),
      '#description' => t('The name of the database on the server.'),
      '#size' => 40,
      '#maxlength' => 120,
      '#default_value' => isset($definition['database']) ? $definition['database'] : '',
      '#states' => array(
        'optional' => array(
          ':input[name="aed_driver"]'
          => array(
            'value' => 'sqlite'),
        ),
        'visible' => array(
          ':input[name="aed_driver"]' => array(
            '!value' => 'sqlite'),
        ),
      ),
    );

    // Text field for the path to a sqlite file.
    $form['aed_file'] = array(
      '#type' => 'textfield',
      '#title' => t('Path to sqlite file.'),
      '#description' => t('The path to the sqlite file.'),
      '#size' => 40,
      '#default_value' => isset($definition['file']) ? $definition['file'] : '',
      '#maxlength' => 120,
      '#states' => array(
        'optional' => array(
          ':input[name="aed_driver"]'
          => array(
            '!value' => 'sqlite'),
        ),
        'visible' => array(
          ':input[name="aed_driver"]' => array(
            'value' => 'sqlite'),
        ),
      ),
    );

    // Text field for the user name.
    $form['aed_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Database Username'),
      '#description' => t('Name of a user with access to this database.'),
      '#default_value' => isset($definition['username']) ? $definition['username'] : '',
      '#size' => 40,
      '#maxlength' => 120,
      '#states' => array(
        'optional' => array(
          ':input[name="aed_driver"]'
          => array(
            'value' => 'sqlite'),
        ),
        'visible' => array(
          ':input[name="aed_driver"]' => array(
            '!value' => 'sqlite'),
        ),
      ),
    );

    // Text field for the password.
    $form['aed_password'] = array(
      '#type' => 'password',
      '#title' => t('Database Password'),
      '#description' => t('Password for the above named user to access the database.'),
      '#size' => 40,
      '#maxlength' => 120,
      '#states' => array(
        'optional' => array(
          ':input[name="aed_driver"]'
          => array(
            'value' => 'sqlite'),
        ),
        'visible' => array(
          ':input[name="aed_driver"]' => array(
            '!value' => 'sqlite'),
        ),
      ),
    );

    // Text field for the server host name.
    $form['aed_host'] = array(
      '#type' => 'textfield',
      '#title' => t('Hostname of the Database Server'),
      '#description' => t('Host name of the database server.'),
      '#default_value' => isset($definition['host']) ? $definition['host'] : '',
      '#size' => 40,
      '#maxlength' => 120,
      '#states' => array(
        'optional' => array(
          ':input[name="aed_driver"]'
          => array(
            'value' => 'sqlite'),
        ),
        'visible' => array(
          ':input[name="aed_driver"]' => array(
            '!value' => 'sqlite'),
        ),
      ),
    );

    // Text field for the server port #.
    $form['aed_port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port # on Server'),
      '#description' => t('Port Number for the Database server.'),
      '#default_value' => isset($definition['port']) ? $definition['port'] : '',
      '#size' => 6,
      '#maxlength' => 6,
      '#states' => array(
        'optional' => array(
          ':input[name="aed_driver"]'
          => array(
            'value' => 'sqlite'),
        ),
        'visible' => array(
          ':input[name="aed_driver"]' => array(
            '!value' => 'sqlite'),
        ),
      ),
    );

    // Text field for the data table prefix.
    $form['aed_prefix'] = array(
      '#type' => 'textarea',
      '#title' => t('Database Table Prefix'),
      '#description' => t('Prefix for tables created in this database.  Use PHP array notation if needed.'),
      '#rows' => 4,
      '#maxlength' => 1024,
      '#default_value' => isset($definition['prefix']) ? $definition['prefix'] : '',
    );

    // Add editing submit buttons.
    if ($dbsettings->in_settings_php == 1) {
      $form['remove_from_settings_php'] = array(
        '#type' => 'submit',
        '#value' => t('Remove from settings.php'),
        '#submit' => array(
          'aed_remove_from_settings_php'),
      );
    }
    elseif ($dbsettings->in_settings_php == 0) {
      $form['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array(
          'aed_edit_form_submit_delete'),
      );
      $form['update'] = array(
        '#type' => 'submit',
        '#value' => t('Update'),
        '#submit' => array(
          'aed_edit_form_submit_update'),
      );
      $form['add_to_settings_php'] = array(
        '#type' => 'submit',
        '#value' => t('Add to settings.php'),
        '#submit' => array(
          'aed_add_to_settings_php'),
      );
    }
    $form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
      '#submit' => array(
        'aed_edit_form_submit_cancel'),
    );
    return $form;
  }
}

/**
 * Form Builder.
 * 
 * Create and display settings for aed Module.
 * 
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function aed_form($form, &$form_state) {
  global $databases;

  $form[] = array(
    '#prefix' => '<h3>',
    '#markup' => 'Databases defined in settings.php:',
    '#suffix' => '</h3>',
  );
  $builtin_header = array(t('PHP Name'), t('Database Name (or sqlite Path)'),
    t('Server'), t('Port'), t('Driver'), t('Prefix'));

  // Print "Databases defined in Settings.php".
  $builtin_rows = array();
  foreach ($databases as $dbkey => $dbsettings) {
    if (isset($dbsettings['default']['aed'])) {
      $col1 = array(
        '#markup' => '<em><strong>' . $dbkey . '</strong></em> '
        . $dbsettings['default']['aed'],
      );
    }
    else {
      $col1 = array(
        '#markup' => '<em><strong>' . $dbkey . '</strong></em>',
      );
    }

    $builtin_rows[] = array_merge((array) $col1, explode('|', aed_display_definition_output($dbsettings['default'])));
  }

  $form[] = array(
    '#theme' => 'table',
    '#header' => $builtin_header,
    '#rows' => $builtin_rows,
  );

  // Print databases defined by this module.
  $qs = 'select * from {aed};';
  $qr = db_query($qs);

  $form[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Databases managed by aed:'),
    '#suffix' => '</h3>',
  );

  // Print "Databases defined in aed database".
  if ($qr->rowCount() > 0) {
    $aed_header = array(t('PHP Name (Click to Edit/Delete)'), t('Database Name (or sqlite Path)'),
      t('Server'), t('Port'), t('Driver'), t('Prefix'));

    $aed_rows = array();
    foreach ($qr as $dbsettings) {
      $path = 'admin/config/development/aed/edit/';
      $path .= $dbsettings->DatabaseHandle;

      $col1 = l($dbsettings->DatabaseHandle, $path);
      $definition = unserialize($dbsettings->DatabaseConnectionInfo);

      $aed_rows[] = array_merge((array) $col1, explode('|', aed_display_definition_output($definition)));
    }

    $form[] = array(
      '#theme' => 'table',
      '#header' => $aed_header,
      '#rows' => $aed_rows,
    );
  }
  else {
    $form[] = array(
      '#markup' => t('--NO DATABASES DEFINED BY aed--'),
    );
  }

  $form[] = array(
    '#prefix' => '<h3>',
    '#markup' => t('Add New Database'),
    '#suffix' => '</h3>',
  );

  // Display form to add new databases.
  $form['aed_database'] = array(
    '#type' => 'machine_name',
    '#title' => t('PHP name (machine_name) of Database to register'),
    '#description' => t('The name of the database as it will be known in php code.'),
    '#size' => 40,
    '#maxlength' => 60,
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'aed_name_exists',
    ),
  );

  // Text field for the database driver.
  $form['aed_driver'] = array(
    '#type' => 'select',
    '#title' => t('Database Driver'),
    '#description' => t('Driver to use in accessing the database.'),
    '#options' => array(
      'mysql' => t('mysql'),
      'mysqli' => t('mysqli'),
      'pgsql' => t('pgsql'),
      'sqlite' => t('sqlite'),
    ),
    '#default_value' => variable_get('aed_driver', 'mysql'),
    '#required' => TRUE,
  );

  // Text field for the database name on the server.
  $form['aed_db'] = array(
    '#type' => 'textfield',
    '#title' => t('Server name of Database to register'),
    '#description' => t('The name of the database on the server.'),
    '#size' => 40,
    '#maxlength' => 120,
    '#states' => array(
      'optional' => array(
        ':input[name="aed_driver"]'
        => array(
          'value' => 'sqlite'),
      ),
      'visible' => array(
        ':input[name="aed_driver"]' => array(
          '!value' => 'sqlite'),
      ),
    ),
  );

  $form[] = array(
    '#markup' => '<script type=\'text/javascript\'>jQuery(\'input[name="aed_database"]\').blur(function() { if (!(jQuery(\'input[name="aed_db"]\').val())){ jQuery(\'input[name="aed_db"]\'). val(jQuery(\'input[name="aed_database"]\').val()); } });</script>',
  );

  // Text field for the path to a sqlite file.
  $form['aed_file'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to sqlite file.'),
    '#description' => t('The path to the sqlite file.'),
    '#size' => 40,
    '#maxlength' => 120,
    '#states' => array(
      'optional' => array(
        ':input[name="aed_driver"]'
        => array(
          '!value' => 'sqlite'),
      ),
      'visible' => array(
        ':input[name="aed_driver"]' => array(
          'value' => 'sqlite'),
      ),
    ),
  );

  // Text field for the user name.
  $form['aed_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Database Username'),
    '#description' => t('Name of a user with access to this database.'),
    '#default_value' => variable_get('aed_user'),
    '#size' => 40,
    '#maxlength' => 120,
    '#states' => array(
      'optional' => array(
        ':input[name="aed_driver"]'
        => array(
          'value' => 'sqlite'),
      ),
      'visible' => array(
        ':input[name="aed_driver"]' => array(
          '!value' => 'sqlite'),
      ),
    ),
  );

  // Text field for the password.
  $form['aed_password'] = array(
    '#type' => 'password',
    '#title' => t('Database Password'),
    '#description' => t('Password for the above named user to access the database.'),
    '#size' => 40,
    '#maxlength' => 120,
    '#states' => array(
      'optional' => array(
        ':input[name="aed_driver"]'
        => array(
          'value' => 'sqlite'),
      ),
      'visible' => array(
        ':input[name="aed_driver"]' => array(
          '!value' => 'sqlite'),
      ),
    ),
  );

  // Text field for the server host name.
  $form['aed_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname of the Database Server'),
    '#description' => t('The Host Name to access this database on.'),
    '#default_value' => variable_get('aed_host'),
    '#size' => 40,
    '#maxlength' => 120,
    '#states' => array(
      'optional' => array(
        ':input[name="aed_driver"]'
        => array(
          'value' => 'sqlite'),
      ),
      'visible' => array(
        ':input[name="aed_driver"]' => array(
          '!value' => 'sqlite'),
      ),
    ),
  );

  // Text field for the server port #.
  $form['aed_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port # on Server'),
    '#description' => t('Port Number to access on the Database server.'),
    '#default_value' => variable_get('aed_port', '3306'),
    '#size' => 6,
    '#maxlength' => 6,
    '#states' => array(
      'optional' => array(
        ':input[name="aed_driver"]'
        => array(
          'value' => 'sqlite'),
      ),
      'visible' => array(
        ':input[name="aed_driver"]' => array(
          '!value' => 'sqlite'),
      ),
    ),
  );

  // Text field for the data table prefix.
  $form['aed_prefix'] = array(
    '#type' => 'textarea',
    '#title' => t('Database Table Prefix'),
    '#description' => t('Prefix for tables created in this database.  Use PHP array notation if needed.'),
    '#rows' => 4,
    '#maxlength' => 1024,
    '#default_value' => variable_get('aed_prefix'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Settings and Add this Database'),
  );

  return $form;
}

/**
 * Form handler for creating new entries.
 * 
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function aed_form_submit($form, &$form_state) {
  if (!user_access('administer site configuration')) {
    return;
  }
  $definition = array();

  variable_set('aed_driver', $form_state['values']['aed_driver']);
  variable_set('aed_port', $form_state['values']['aed_port']);
  variable_set('aed_user', $form_state['values']['aed_user']);
  variable_set('aed_host', $form_state['values']['aed_host']);
  variable_set('aed_prefix', $form_state['values']['aed_prefix']);

  if ($form_state['values']['aed_driver'] === 'mysql' ||
      $form_state['values']['aed_driver'] === 'mysqli' ||
      $form_state['values']['aed_driver'] === 'pgsql') {
    $definition['database'] = $form_state['values']['aed_db'];
    $definition['username'] = $form_state['values']['aed_user'];
    $definition['password'] = $form_state['values']['aed_password'];
    $definition['host'] = $form_state['values']['aed_host'];
    $definition['port'] = $form_state['values']['aed_port'];
    $definition['driver'] = $form_state['values']['aed_driver'];
    $definition['prefix'] = $form_state['values']['aed_prefix'];
  }
  elseif ($form_state['values']['aed_driver'] === 'sqlite') {
    $definition['file'] = $form_state['values']['aed_file'];
    $definition['driver'] = $form_state['values']['aed_driver'];
    $definition['prefix'] = $form_state['values']['aed_prefix'];
  }

  db_insert('aed')->fields(array(
    'DatabaseHandle' => $form_state['values']['aed_database'],
    'DatabaseConnectionInfo' => serialize($definition),
      )
  )->execute();

  drupal_set_message(t('The settings have been saved.'));
}

/**
 * Updates the entry in the database.
 * 
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function aed_edit_form_submit_update($form, &$form_state) {
  if (!user_access('administer site configuration')) {
    return;
  }
  $definition = array();

  variable_set('aed_driver', $form_state['values']['aed_driver']);
  variable_set('aed_port', $form_state['values']['aed_port']);
  variable_set('aed_user', $form_state['values']['aed_user']);
  variable_set('aed_host', $form_state['values']['aed_host']);
  variable_set('aed_prefix', $form_state['values']['aed_prefix']);

  if ($form_state['values']['aed_driver'] === 'mysql' ||
      $form_state['values']['aed_driver'] === 'mysqli' ||
      $form_state['values']['aed_driver'] === 'pgsql') {
    $definition['database'] = $form_state['values']['aed_db'];
    $definition['username'] = $form_state['values']['aed_user'];
    $definition['password'] = $form_state['values']['aed_password'];
    $definition['host'] = $form_state['values']['aed_host'];
    $definition['port'] = $form_state['values']['aed_port'];
    $definition['driver'] = $form_state['values']['aed_driver'];
    $definition['prefix'] = $form_state['values']['aed_prefix'];
  }
  elseif ($form_state['values']['aed_driver'] === 'sqlite') {
    $definition['file'] = $form_state['values']['aed_file'];
    $definition['driver'] = $form_state['values']['aed_driver'];
  }

  db_update('aed')->fields(array(
        'DatabaseHandle' => $form_state['values']['aed_database'],
        'DatabaseConnectionInfo' => serialize($definition),
      ))
      ->condition('DatabaseHandle', $form_state['values']['aed_old_database'], '=')
      ->execute();

  drupal_set_message(t("The '<strong>@dbname</strong>' database definition has been updated.", array(
    '@dbname' => $form_state['values']['aed_old_database'])));

  drupal_goto('admin/config/development/aed');
}

/**
 * Delete the selected entry from the database.
 * 
 * @param array $form
 *   Form Array
 * 
 * @param array $form_state
 *   Form State Array
 */
function aed_edit_form_submit_delete($form, &$form_state) {
  if (!user_access('administer site configuration')) {
    return;
  }
  db_delete('aed')
      ->condition('DatabaseHandle', $form_state['values']['aed_database']
      )->execute();

  drupal_set_message(t("aed '<strong>@dbname</strong>' database definition deleted.", array(
    '@dbname' => $form_state['values']['aed_database'])));
  drupal_goto('admin/config/development/aed');
}

/**
 * Cancel's the edit form and returns to the main form.
 * 
 * @param array $form
 *   Form array
 * @param array $form_state
 *   Form State array
 */
function aed_edit_form_submit_cancel($form, &$form_state) {
  if (!user_access('administer site configuration')) {
    return;
  }
  drupal_set_message(t('aed update/delete cancelled.'));
  drupal_goto('admin/config/development/aed');
}

/**
 * Helper function to display a formatted row of information.
 * 
 * @param array $row
 *   Row of database information to display.
 */
function aed_display_definition_output($row) {
  $return_array = array();

  foreach ($row as $setting_tag => $setting_value) {
    if ($setting_tag === 'password' ||
        $setting_tag === 'username' ||
        $setting_tag === 'aed') {
      continue;
    }
    elseif ($setting_tag === 'file') {
      $return_array[] = $setting_value;
      $return_array[] = 'localhost';
      $return_array[] = '-';
    }
    else {
      $return_array[] = $setting_value;
    }
  }
  return implode('|', $return_array);
}

/**
 * Function to append item managed by aed onto settings.php.
 * 
 * @param array $form
 *   Form array
 * @param array $form_state
 *   Form state array
 */
function aed_add_to_settings_php($form, $form_state) {
  if (!user_access('administer site configuration')) {
    return;
  }
  $settings_php_path = conf_path() . '/settings.php';
  $focus_database = $form_state['values']['aed_old_database'];
  $query = db_select('aed', 'a')
      ->fields('a', array('DatabaseHandle', 'DatabaseConnectionInfo',
        'in_settings_php'))
      ->condition('DatabaseHandle', $focus_database, '=');
  $qr = $query->execute()->fetchObject();

  if ($qr->in_settings_php != 0) {
    drupal_set_message(
        t('aed believes @database is already in settings.php, it will not be readded.', array(
      '@database' => $settings_php_path,)), 'warning');
    return;
  }
  else {
    if (!($contents = file_get_contents($settings_php_path))) {
      drupal_set_message(t('Unable to open <i>@database</i>!', array(
        '@database' => $settings_php_path,)), 'error');
      return;
    }
    if (!($fp = fopen($settings_php_path, 'a'))) {
      drupal_set_message(t('Unable to open <i>@database</i>!', array(
        '@database' => $settings_php_path,)), 'error');
      return;
    }
    else {
      drupal_set_message(t('<i>@database</i> opened successfully.', array(
        '@database' => $settings_php_path,)));
      if (db_update('aed')
              ->fields(array(
                'in_settings_php' => 1))
              ->condition('DatabaseHandle', $focus_database, '=')
              ->execute()) {
        drupal_set_message(t('<i>@database</i> status updated in database.', array(
          '@database' => $settings_php_path,)));

        // Construct text to add to settings.php.
        if (substr($contents, -1) !== "\n") {
          $dbinfo = "\n/****start: added by aed: {";
        }
        else {
          $dbinfo = "/****start: added by aed: {";
        }
        $dbinfo .= $focus_database . "}****/\n";
        $dbinfo .= '$databases[\'' . $focus_database;
        $dbinfo .= "']['default'] = array(\n";
        $dbinfo .= "  'aed' => 'managed by aed',\n";
        $definition = unserialize($qr->DatabaseConnectionInfo);
        foreach ($definition as $key => $value) {
          $dbinfo .= "  '$key' => '$value',\n";
        }
        $dbinfo .= ");\n";
        $dbinfo .= "/****end: added by aed: {";
        $dbinfo .= $focus_database . "}****/";

        if (fwrite($fp, $dbinfo) && fclose($fp)) {
          drupal_set_message(t('<i>@database</i> written and closed successfully.', array(
            '@database' => $settings_php_path,)));
        }
        else {
          if (!(db_update('aed')->fields(array(
                'in_settings_php' => 1))->condition(array(
                'DatabaseHandle' => $focus_database))->execute())) {
            drupal_set_message(
                t('<i>@focus</i> status reverted in database, unable to update @database.', array(
              '@database' => $settings_php_path,
              '@focus' => $focus_database)
                ), 'error');
          }
          else {
            drupal_set_message(t('@focus database status updated in aed database.', array(
              '@focus' => $focus_database)));
          }
        }
      }
    }
  }
  drupal_goto('admin/config/development/aed');
}

/**
 * Edits the settings.php file to remove a database.
 * 
 * @param array $form
 *   Form array
 * @param array $form_state
 *   Form State array
 */
function aed_remove_from_settings_php($form, $form_state) {
  if (!user_access('administer site configuration')) {
    return;
  }
  $settings_php_path = conf_path() . '/settings.php';
  $focus_database = $form_state['values']['aed_old_database'];

  if (!($contents = file_get_contents($settings_php_path))) {
    drupal_set_message(t('Unable to open <i>@path</i>!', array('@path' => $settings_php_path)), 'error');
    return;
  }
  drupal_set_message(t('@path read successfully.', array('@path' => $settings_php_path)));

  $start_tag = "/****start: added by aed: {" . $focus_database . "}****/\n";
  $end_tag = "/****end: added by aed: {" . $focus_database . "}****/";
  $start_of_deletion = strpos($contents, $start_tag);
  $end_of_deletion = strpos($contents, $end_tag) + strlen($end_tag);

  if ($start_of_deletion === FALSE) {
    drupal_set_message(t('@tag not found in @path no changes made.', array(
      '@tag' => $start_tag,
      '@path' => $settings_php_path,)), 'warning');
    return;
  }

  if ($end_of_deletion === FALSE) {
    drupal_set_message(t('@tag not found in @path no changes made.', array(
      '@tag' => $end_tag,
      '@path' => $settings_php_path,)), 'warning');
    return;
  }
  $contents = substr($contents, 0, $start_of_deletion) .
      substr($contents, $end_of_deletion);

  if (!($fp = fopen($settings_php_path, 'w'))) {
    drupal_set_message(t('Unable to open @path for write.', array(
      '@path' => $settings_php_path)), 'error');
  }
  drupal_set_message($settings_php_path . ' opened successfully for write.');
  if (!(fwrite($fp, $contents)) || !(fclose($fp))) {
    drupal_set_message(t('Unable to write to @path.', array(
      '@path' => $settings_php_path)), 'error');
  }
  drupal_set_message(t('@path successfully written and closed.', array(
    '@path' => $settings_php_path)));

  if (db_update('aed')
          ->fields(array(
            'in_settings_php' => 0))
          ->condition('DatabaseHandle', $focus_database, '=')
          ->execute()) {
    drupal_set_message(t('@focus database status updated in aed database.', array(
      '@focus' => $focus_database)));
  }
  drupal_goto('admin/config/development/aed');
}

/**
 * Tests to see if the proposed machine name exists.
 * 
 * @param string $name 
 *   Name of database which might exist in aed.
 * 
 * @return int
 *   Return 1 if database handle exists, otherwise return null.
 */
function aed_name_exists($name) {
  // Check to see if item exists in databases array.
  global $databases;
  if (isset($databases[$name])) {
    return 1;
  }
  // Check to see if item is in database.
  $query = db_select('aed', 'a')
      ->fields('a', array('DatabaseHandle'))
      ->condition('DatabaseHandle', $name, '=');
  return $query->execute()->rowCount();
}
